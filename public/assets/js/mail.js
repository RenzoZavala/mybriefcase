const $form   = document.querySelector('#mailForm')
$form.addEventListener('submit',handlesubmit)
async function handlesubmit(event){
    event.preventDefault();
    $('#mailModal').modal('show');
    $('.div-loading').removeClass('d-none');
    $('.div-success').addClass('d-none');
    $('.div-error').addClass('d-none');
    $('.div-validate').addClass('d-none');
    const form = new FormData(this)
    const response = await fetch(this.action, {
        method : this.method,
        body: form,
        headers: {
            'Accept' : 'application/json'
        }
    })
    if(response.ok)
    {
        $('.div-loading').addClass('d-none');
        $('.div-success').removeClass('d-none');
        $('.div-error').addClass('d-none');
        $('.div-validate').addClass('d-none');

        $('#nameForm').val('');
        $('#emailForm').val('');
        $('#messageForm').val('');
    }
    else{
        console.log(response)
        if(response.status === 422)
        {
            $('.div-loading').addClass('d-none');
            $('.div-success').addClass('d-none');
            $('.div-error').addClass('d-none');
            $('.div-validate').removeClass('d-none');
        }
        else
        {
            $('.div-loading').addClass('d-none');
            $('.div-success').addClass('d-none');
            $('.div-error').removeClass('d-none');
            $('.div-validate').addClass('d-none');
            
            // $('#nameForm').val('');
            // $('#emailForm').val('');
            // $('#messageForm').val('');
        }
    }
}